#!/bin/bash

# === changelog
# - 2020-11-25-1
# * adds '${initnorecord}' to allow for initial jamulus setup without recording enabled
# * removes --servername from jamulus startup, as this option was removed from jamulus and broke updates
# * fixes typo in schedules, adds better vm name


# need a time to force custom script extension to be reinstalled
runepoch=$(date +%s)

cat <<- EOINTRO > ./readme_$runepoch.txt

	tigabeatz | kreaterra | jamulus 
	-------------------------
	Creates resources needed to run a jamulus server and installs jamulus per virtual machine extension. 
	
	Jamulus is setup to record each session into a storage accounts file share, to be accessed later. 
	Recording can be deactivated on initial setup or update. 
	
	No manual activities on the server should be needed. Secure, update and upgrade your Server via Azure Portal then. 

	git: https://gitlab.com/tigabeatz/jamulus_server_on_azure

	version: 2020-11-25-1
	
	Based on examples from https://docs.microsoft.com/de-de/cli/azure/?view=azure-cli-latest, 
	https://stackoverflow.com/questions/tagged/shell and https://stackoverflow.com/questions/tagged/azure

	How to
	-------------------------
	Log into https://portal.azure.com 
	Download the script to a Linux flavored cloud shell in azure portal 

	git clone https://gitlab.com/tigabeatz/jamulus_server_on_azure.git
	cd jamulus_server_on_azure

	Allow this script to be executed
	sudo chmod +x install_jamulus.sh
	
	Run this script
	./install_jamulus.sh

		Settings you will be asked for:
		- The location where to run your resources: https://azure.microsoft.com/en-us/global-infrastructure/locations/
		- A dns name: this will be the first part of your servers address
		- Jamulus server info details: https://github.com/corrados/jamulus/wiki/Command-Line-Options

	Update 
	-------------------------
	Run the script again

	How to change settings
	-------------------------
	Start reading the script from the last line and go upwards. You´ll spot where you want to change stuff. :-)

	E.g. If you like to use accelerated networking: Supported VM series include D/DSv2, D/DSv3, E/ESv3, F/FS, FSv2, and Ms/Mms. 
	Look for the command "az vm create" and change the parameter --size ..... 

	Schedules:
	To start and stop a server at given times, set certain environment variables. 
	
	-     jamschedulelocation="westeurope"
				Scheduling works by utilizing Logig Apps. These are not available in every location.  
				Microsoft.Logic/workflows'. List of available regions for the resource type is 'northcentralus,centralus,southcentralus,northeurope,westeurope,eastasia,southeastasia,westus,eastus,eastus2,japanwest,japaneast,brazilsouth,australiaeast,australiasoutheast,southindia,centralindia,westindia,canadacentral,canadaeast,westus2,westcentralus,uksouth,ukwest,francecentral,francesouth,koreacentral,koreasouth,southafricanorth,southafricawest,uaecentral'
	- 	 jamstarttime="2020-05-01T14:00:00Z"
	- 	 jamendtime="2020-05-01T21:30:00Z"

	Hint:
	After a successfull run of the script, a file named apply_settings.sh is created and your settings are persisted there. This IS needed to run start / stop the vm on a schedule.
	
		Set the parameter jamscheduled=true, to enable schedules and follow instructions from the generated readme file

	jamscheduled=false
	jamschedulelocation="westeurope"
	jamstarttime="2020-05-01T14:00:00Z"
	jamendtime="2020-05-01T21:30:00Z"
	jamlocationname="germanywestcentral"
	jamdnsname="anychord"
	jamserverinfo="Jamnode,Frankfurt,82"
	jamwithoutrecording=no
	
	Record:
	Deactivate recording with setting jamwithoutrecording=yes

	Apply these settings with " source ./apply_settings.sh " and then run ./install_jamulus.sh so you do not have to answer any questions asked by the script. :-)

	Update Jamulus Settings only
	----------------------------

	Use the Run Command option in the Azure Portal for the Virtual Machine and 
	take the code below as a copy / paste example.

	------------->
	# jamulus server config update
	jamuluscfgupdate() {
		echo "updating Jamulus config"

		cp /etc/systemd/system/jamulus.service ./jamulus.service.old
		cp /etc/systemd/system/jamulus.service ./jamulus.service.edit

		oldcfg=\$(cat ./jamulus.service.edit | grep ExecStart=)

		echo "changed from " 
		echo $oldcfg

		sed -i "s@\${oldcfg}@\${newcfg}@g" ./jamulus.service.edit

		echo "to"
		cat ./jamulus.service.edit | grep ExecStart=

		echo "applying config and restarting jamulus service"
		systemctl stop jamulus.service
		cp ./jamulus.service.edit /etc/systemd/system/jamulus.service -f
		systemctl daemon-reload
		systemctl enable jamulus.service
		systemctl start --no-block jamulus.service
		echo ""
		systemctl -l status jamulus.service
	}

	# set new config here
	export  newcfg="ExecStart=/usr/local/bin/Jamulus --nogui --server --port 22124 --centralserver localhost --welcomemessage 'Another New Jamulus Server' --serverinfo 'Less20Char,Frankfurt,82' --recording '/mnt/bmajstore/bmajshare/recordings' --htmlstatus '/var/www/html/jamstatus.html'"

	jamuluscfgupdate
	<------------

	Recap:
	A readme file is created after each sucessfull run, containing the manual and created urls, setup strings, ...

EOINTRO

timestamp() {
	date +"%T"
}

initinstaller() {
	rc=0
	rrc=0

	# force=1 toggle this parameter to try to update exisitng resources (does not apply to vm extension), or not
	force=1

	echo $runepoch $rc $rrc $force

	# check if resource exist, setup or list az cli / subscription

	# subscription
	export jamsubscriptionid=$(az account list --query '[*].id' | tr -d '"][\t\n ')

	if [ ! $jamsubscriptionid ]; then
		echo 'execute > az login < and setup your azure subscription with > az account set --subscription "My subscriptions name" < then run this script again'
		exit 1
	else  
		echo " using subscription $jamsubscriptionid " 
	fi
	
	# check if relevant variables are set or use defaults or ask user

	echo ""
	if [ ! $jamlocationname ]; then
		read -e -p "Azure location name: " -i "germanywestcentral" jamlocationname
		echo $jamlocationname
		# export jamlocationname="germanywestcentral"
	fi

	echo ""
	if [ ! $jamdnsname ]; then
		read -e -p "DNS prefix: " -i "adur" jamdnsname
		echo $jamdnsname
		# export jamdnsname="cmoll"
	fi

	echo ""
	if [ ! $jamserverinfo ]; then
		read -e -p "Jamulus Server Info String: " -i "Jamnode,Frankfurt,82" jamserverinfo
		echo $jamserverinfo
		# export jamserverinfo="Jamnode,Frankfurt,82"
	fi
	
	echo ""
	if [ ! $jamwithoutrecording ]; then
		read -e -p "type yes if you want to disable recording, anything else keeps it activated: " -i "no" jamwithoutrecording
		echo $jamwithoutrecording
		# export jamwithoutrecording="yes"
	fi

	# resources

	# disable recording on startup. this extends the jamulus startup command in the service configuration file 
	if [ ${jamwithoutrecording} == "yes" ] ; then
		initnorecord="--norecord"
	else
		initnorecord=""
	fi
	
	export jamstorename="${jamdnsname}store"
	export jamsharename="${jamdnsname}share"
	export jamgroupname="${jamdnsname}group"
	export jamserverrecpath=/mnt/${jamstorename}/${jamsharename}/recordings
	export jamvmname="${jamdnsname}001"								

	# what is configured?
	echo ""
	env | grep jam

	# check if group, vm and storage account exist
	az group exists --name ${jamgroupname}
	
	echo ""
	if  [ "$(az storage account check-name --name ${jamstorename} --query 'nameAvailable')" == "false" ]; then
		echo "[INFO] $(timestamp) Storage account exists"
		rrc=2
	fi

	echo ""
	if [ $(az vm show -g ${jamgroupname} --name ${jamvmname} --query "id" ) ]; then
		rrc=3
		echo "[INFO] $(timestamp) Virtual machine exists"
	fi

	echo ""
	if [ $rrc == 0 ]; then
		echo -e "\e[34m[INFO] $(timestamp) all targeted resources available"
	else
		echo -e "\e[32m[INFO] $(timestamp) If accessible, existing resources will be updated and missing resources will be created. If not accessible these steps will fail. Stop the script with ctrl+c and restart with updated jamdnsname"
		echo "  waiting for 15 seconds"
		sleep 15
		echo "  proceeding "
	fi
}

getout() {

	# cleaning up
	rm ./install_jamnode.sh
	rm ./public.json
	rm ./schedule_up.json 
	rm ./schedule_down.json 

	connectionString=$(az storage account show-connection-string --resource-group ${jamgroupname} --name ${jamstorename} --query "connectionString" | tr -d '"')
	sas=$(az storage share generate-sas --connection-string $connectionString --expiry 2029-02-01T12:20Z --name ${jamsharename} --permissions dlrw)

	cat <<- EOOUTRO >> ./readme_$runepoch.txt

		- SSH login: ssh $(whoami)@${jamdnsname}.${jamlocationname}.cloudapp.azure.com
		you need to open the SSH port in the VMs NSG to connect. Disabled on default.
		- Server status page: http://${jamdnsname}.${jamlocationname}.cloudapp.azure.com
		- Jamulus connection string: ${jamdnsname}.${jamlocationname}.cloudapp.azure.com:22124

		Handle your recorded sessions: 
		https://docs.microsoft.com/de-de/azure/storage/files/storage-how-to-use-files-windows
		https://docs.microsoft.com/de-de/azure/vs-azure-tools-storage-manage-with-storage-explorer?tabs=windows

		Your sas key: ${sas}

		Update with this installations settings:

			source ./apply_settings_$runepoch.sh
			./install_jamulus.sh

		Happy jamming.

		Btw: You may want to check the fileshares size and quota frequently using the azure portal

	EOOUTRO

	echo "execute cat ./readme_$runepoch.txt or open readme_$runepoch.txt to see how to interact with your resources" 

	cat <<- LAZYSETTINGS > ./apply_settings_$runepoch.sh
		export jamscheduled=false
		export jamschedulelocation=${jamlocationname}
		export jamstarttime="2020-05-01T14:00:00Z"
		export jamendtime="2020-05-01T21:30:00Z"
		export jamlocationname=${jamlocationname}
		export jamdnsname=${jamdnsname}
		export jamserverinfo=${jamserverinfo}
		export jamwithoutrecording=${jamwithoutrecording}
		export jamvmname=${jamvmname}

		echo "set configs: "
		env | grep jam

	LAZYSETTINGS
	
	echo "for conveniance you may use the files containing _latest within their names."
	
	cp ./apply_settings_$runepoch.sh ./apply_settings_latest.sh
	cp ./readme_$runepoch.txt ./readme_latest.txt

}

createfiles() {
	# schedules vm up / down
	cat <<- EOSCHED > ./schedule_up.json 
			{
			"\$schema": "https://schema.management.azure.com/schemas/2019-04-01/deploymentTemplate.json#",
			"contentVersion": "1.0.0.0",
			"parameters": {},
			"variables": {},
			"resources": [{
					"type": "Microsoft.Logic/workflows",
					"apiVersion": "2019-05-01",
					"name": "scheduledvmup",
					"location": "$jamschedulelocation",
					"identity": {
						"type": "SystemAssigned"
					},
					"properties": {
						"state": "Enabled",
						"definition": {
							"\$schema": "https://schema.management.azure.com/providers/Microsoft.Logic/schemas/2016-06-01/workflowdefinition.json#",
							"actions": {
								"AC": {
									"inputs": {
										"variables": [{
											"name": "ApiCallString",
											"type": "string",
											"value": "https://management.azure.com/subscriptions/@{variables('suid')}/resourceGroups/@{variables('rgname')}/providers/Microsoft.Compute/virtualMachines/@{variables('vmname')}/start?api-version=2019-07-01"
										}]
									},
									"runAfter": {
										"SU": [
											"Succeeded"
										]
									},
									"type": "InitializeVariable"
								},
								"HTTP": {
									"inputs": {
										"authentication": {
											"type": "ManagedServiceIdentity"
										},
										"method": "POST",
										"retryPolicy": {
											"count": 3,
											"interval": "PT20S",
											"type": "fixed"
										},
										"uri": "@variables('ApiCallString')"
									},
									"runAfter": {
										"AC": [
											"Succeeded"
										]
									},
									"type": "Http"
								},
								"RG": {
									"inputs": {
										"variables": [{
											"name": "rgname",
											"type": "string",
											"value": "$jamgroupname"
										}]
									},
									"runAfter": {
										"VM": [
											"Succeeded"
										]
									},
									"type": "InitializeVariable"
								},
								"SU": {
									"inputs": {
										"variables": [{
											"name": "suid",
											"type": "string",
											"value": "$jamsubscriptionid"
										}]
									},
									"runAfter": {
										"RG": [
											"Succeeded"
										]
									},
									"type": "InitializeVariable"
								},
								"VM": {
									"inputs": {
										"variables": [{
											"name": "vmname",
											"type": "string",
											"value": "${jamvmname}"
										}]
									},
									"runAfter": {},
									"type": "InitializeVariable"
								}
							},
							"contentVersion": "1.0.0.0",
							"outputs": {},
							"parameters": {},
							"triggers": {
								"Recurrence": {
									"recurrence": {
										"frequency": "Day",
										"interval": 1,
										"startTime": "$jamstarttime"
									},
									"type": "Recurrence"
								}
							}
						},
						"parameters": {}
					}
				}

			],
			"outputs": {
				"principalId": {
					"type": "string",
					"value": "[reference(resourceId('Microsoft.Logic/workflows/', 'scheduledvmup'), '2019-05-01', 'Full').Identity.principalId]"
				}
			}
		}

	EOSCHED

	cat <<- EOSCHED > ./schedule_down.json 
			{
			"\$schema": "https://schema.management.azure.com/schemas/2019-04-01/deploymentTemplate.json#",
			"contentVersion": "1.0.0.0",
			"parameters": {},
			"variables": {},
			"resources": [{
					"type": "Microsoft.Logic/workflows",
					"apiVersion": "2019-05-01",
					"name": "scheduledvmdown",
					"location": "$jamschedulelocation",
					"identity": {
						"type": "SystemAssigned"
					},
					"properties": {
						"state": "Enabled",
						"definition": {
							"\$schema": "https://schema.management.azure.com/providers/Microsoft.Logic/schemas/2016-06-01/workflowdefinition.json#",
							"actions": {
								"AC": {
									"inputs": {
										"variables": [{
											"name": "ApiCallString",
											"type": "string",
											"value": "https://management.azure.com/subscriptions/@{variables('suid')}/resourceGroups/@{variables('rgname')}/providers/Microsoft.Compute/virtualMachines/@{variables('vmname')}/deallocate?api-version=2019-07-01"
										}]
									},
									"runAfter": {
										"SU": [
											"Succeeded"
										]
									},
									"type": "InitializeVariable"
								},
								"HTTP": {
									"inputs": {
										"authentication": {
											"type": "ManagedServiceIdentity"
										},
										"method": "POST",
										"retryPolicy": {
											"count": 3,
											"interval": "PT20S",
											"type": "fixed"
										},
										"uri": "@variables('ApiCallString')"
									},
									"runAfter": {
										"AC": [
											"Succeeded"
										]
									},
									"type": "Http"
								},
								"RG": {
									"inputs": {
										"variables": [{
											"name": "rgname",
											"type": "string",
											"value": "$jamgroupname"
										}]
									},
									"runAfter": {
										"VM": [
											"Succeeded"
										]
									},
									"type": "InitializeVariable"
								},
								"SU": {
									"inputs": {
										"variables": [{
											"name": "suid",
											"type": "string",
											"value": "$jamsubscriptionid"
										}]
									},
									"runAfter": {
										"RG": [
											"Succeeded"
										]
									},
									"type": "InitializeVariable"
								},
								"VM": {
									"inputs": {
										"variables": [{
											"name": "vmname",
											"type": "string",
											"value": "${jamvmname}"
										}]
									},
									"runAfter": {},
									"type": "InitializeVariable"
								}
							},
							"contentVersion": "1.0.0.0",
							"outputs": {},
							"parameters": {},
							"triggers": {
								"Recurrence": {
									"recurrence": {
										"frequency": "Day",
										"interval": 1,
										"startTime": "$jamendtime"
									},
									"type": "Recurrence"
								}
							}
						},
						"parameters": {}
					}
				}

			],
			"outputs": {
				"principalId": {
					"type": "string",
					"value": "[reference(resourceId('Microsoft.Logic/workflows/', 'scheduledvmdown'), '2019-05-01', 'Full').Identity.principalId]"
				}
			}
		}

	EOSCHED

	# script which will be executed on the virtual machine
	cat <<- EOINSTALL > ./install_jamnode.sh
		#!/bin/bash

		# todo: set timezone, use ssl, check performance (accelerated networking and recording to file share), secure webserver, ...

		rc=0

		timestamp() {
		date +"%T"
		}

		check_locks()
		{
		echo "[INFO] \$(timestamp) checking locks"
		whoami
		ls
		t=30
		while fuser /var/{lib/{dpkg,apt/lists},cache/apt/archives}/lock >/dev/null 2>&1; do
			sleep $t
			echo "[INFO] \$(timestamp) waiting ${t} seconds for other proceses to finish. package management still locked"
		done
		}

		echo "[INFO] \$(timestamp) if jamulus was installed previously and is running it will be stopped and upgraded" 
		systemctl stop jamulus.service
		
		echo '[INFO] \$(timestamp) installing build packages, apache, git'
		check_locks
		echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
		apt-get --yes update
		apt-get --yes install build-essential qt5-qmake qtdeclarative5-dev qt5-default qttools5-dev-tools apache2 git-core software-properties-common python3-pip apt-transport-https

		echo '[INFO] \$(timestamp) installing jamulus, create user and local recording folder'
		check_locks
		git clone https://github.com/corrados/jamulus.git jamulus
		cd jamulus
		qmake 'CONFIG+=nosound' Jamulus.pro
		make clean 
		make
		cp -f Jamulus /usr/local/bin/
		cd ..
		adduser --system --no-create-home jamulus
		
		echo '[INFO] \$(timestamp) installing azure cli'
		check_locks
		pip3 --quiet install azure-cli
		
		echo '[INFO] \$(timestamp) creating files'

		# create jamulus service file, index.html
		cat <<- EOF > /etc/systemd/system/jamulus.service
			[Unit]
			Description=Jamulus-Server
			After=network.target

			[Service]
			Type=simple
			User=jamulus
			NoNewPrivileges=true
			ProtectSystem=true
			ProtectHome=true
			Nice=-20
			IOSchedulingClass=realtime
			IOSchedulingPriority=0

			ExecStart=/usr/local/bin/Jamulus --nogui --server --fastupdate --port 22124 --centralserver localhost --welcomemessage 'Jamulus Server' --serverinfo '${jamserverinfo}' --recording '${jamserverrecpath}' --htmlstatus '/var/www/html/jamstatus.html' --discononquit --multithreading ${initnorecord}

			Restart=on-failure
			RestartSec=30
			StandardOutput=journal
			StandardError=inherit
			SyslogIdentifier=jamulus

			[Install]
			WantedBy=multi-user.target
			EOF

		chmod 644 /etc/systemd/system/jamulus.service

		cat <<- EOH > /var/www/html/index.html
				<html><body>
				<svg
				xmlns:dc="http://purl.org/dc/elements/1.1/"
				xmlns:cc="http://creativecommons.org/ns#"
				xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
				xmlns:svg="http://www.w3.org/2000/svg"
				xmlns="http://www.w3.org/2000/svg"
				id="svg8"
				version="1.1"
				viewBox="0 0 75.629805 72.9101"
				height="300"
				width="300"
				style="border:1px solid black;" 
				style="display: inline-block;" >
				<title
					id="title4911">jamserver.logo</title>
				<defs
					id="defs2" />
				<g
					transform="translate(-12.015762,25.19524)"
					id="layer1">
					<g
					id="g4888">
					<path
						style="fill:#ff5555;stroke-width:0.11539393"
						d="M 30.083845,47.414125 C 26.86969,48.714797 25.868899,45.443257 26.505514,44.322102 22.006264,41.607547 14.659656,41.022626 12.468914,36.60644 11.380586,31.385906 12.657054,26.104245 12.18419,20.856436 12.15684,12.69338 11.958537,4.5298008 12.14567,-3.6329742 c 1.83726,-4.2939325 8.342284,-6.1568385 11.119518,-9.8359518 2.376971,-4.163949 -7.068717,-5.094695 -4.54559,-8.90417 2.310063,-2.873337 7.595085,-3.072811 11.677255,-2.658227 15.288702,-0.197921 30.601079,-0.023 45.869077,0.470585 5.486206,0.819656 5.18724,5.905554 0.678371,7.323488 -3.140475,4.495119 4.070133,7.3536983 7.62953,10.0918414 4.076287,3.0713099 2.957461,7.35330637 2.855824,11.1653306 -0.284532,10.673166 -0.07979,21.348405 -0.08675,32.022503 -1.621038,4.022343 -7.575683,5.74369 -12.593306,6.999634 0.443857,2.446015 -0.695922,4.829439 -4.761069,3.884214 -8.638714,0.202365 -17.306311,-0.340795 -25.91579,0.389067 -4.660893,0.119301 -9.325754,0.1238 -13.988929,0.09878 z m 25.218342,-1.356564 c -0.242454,-0.553365 -0.416242,0.429357 0,0 z M 41.574968,45.897886 c 3.771526,0.0017 -2.446752,-0.107624 0,0 z m 16.139583,0.09134 c 0.347698,-0.971753 -1.902492,1.062514 0,0 z M 38.80803,45.868084 c -0.83218,-0.605646 -1.565345,0.922396 0,0 z m 9.201285,-0.0052 c -0.570067,-0.553348 -0.827204,0.671943 0,0 z m 4.109101,-0.0026 c 1.804258,0.590655 6.250924,-0.950576 1.919548,-0.631069 -1.409229,-0.180349 -5.527345,0.81819 -1.919548,0.631069 z m 6.108731,0.0092 c 0.151896,-0.503103 -0.439848,0.310233 0,0 z m 2.732131,-0.09829 c -1.238976,-0.646452 -1.882115,0.541019 0,0 z m -1.703942,-0.006 c 0.614854,-0.866452 0.698534,0.551816 0,0 z m -22.764669,-0.04881 c -0.04238,-0.473974 -2.011005,1.095063 0,0 z m 25.72953,0.07917 c -0.573369,-0.49269 -0.479937,0.308378 0,0 z m -26.956368,-0.17675 c -1.462391,-0.636872 -1.156706,0.266007 0,0 z m 10.32589,0.07657 c -1.379469,-0.388833 -0.542433,0.242872 0,0 z m 18.152664,-0.09001 c -0.877749,-0.452423 -0.858594,0.615563 0,0 z m -30.250652,-0.08457 c -1.643439,-0.765489 -0.891366,0.482345 0,0 z m 14.213916,0.07604 c -1.508892,-0.427783 -0.847927,0.272952 0,0 z m 17.002302,-0.02714 c -0.18136,-0.474425 -0.819968,1.031934 0,0 z m 1.158682,-0.09769 c -1.779152,-0.519372 -0.09066,0.364418 0,0 z m -16.562316,-0.09801 c -0.426267,-0.629476 -0.612877,0.464873 0,0 z m 18.179073,-0.04489 c 5.831934,-0.611294 10.027642,-3.956978 15.427143,-5.361594 1.6129,-0.700907 5.032494,-4.280145 3.537837,-4.481456 -2.608811,2.152331 -4.468551,4.928259 -8.34467,6.112494 -2.748864,1.056588 -6.230998,1.309162 -6.401135,2.967588 -1.341611,0.395878 -2.993207,0.221462 -4.219175,0.762968 z m -35.968231,-0.05282 c -2.198603,0.952019 -4.763451,-2.45757 -1.324593,-1.255223 -3.915263,-1.003325 -8.13505,-1.807193 -11.398547,-3.9141 -2.539533,-1.346237 -4.775111,-3.131532 -5.720349,-5.330576 -0.6112,4.669109 6.835428,6.157352 11.323101,8.006145 2.150438,1.082977 4.308158,2.544329 7.120388,2.493754 z m 27.603866,0 c 0.460008,0.347016 0.487638,0.367863 0,0 z m -13.801936,-0.06995 c -1.379455,-0.388832 -0.542436,0.242882 0,0 z m -3.135254,-0.09769 c -1.379495,-0.388814 -0.542419,0.242882 0,0 z m 17.445777,0.0026 c -0.465691,-0.452753 -0.426411,0.298531 0,0 z M 40.511972,45.004522 c -0.934656,-1.310494 -2.1196,0.835442 0,0 z m 20.685858,0.012 c -0.35633,-0.430062 -0.385315,0.283103 0,0 z M 48.043396,44.91238 c -1.070194,-0.285626 -0.819073,0.29845 0,0 z m 7.565504,-6.26e-4 c -1.439749,-0.240541 -0.993448,0.233197 0,0 z m 6.165803,-0.02279 c -0.550475,-0.340405 -2.216347,0.02114 -0.685763,0.04585 z m -17.756212,-0.07456 c -1.221862,-0.319994 -0.721941,0.326492 0,0 z m 14.041646,6.26e-4 c -1.398591,-0.516039 -0.609742,0.312669 0,0 z m 3.44878,-4.661868 c 1.615104,-0.896197 -1.521856,0.355048 0,0 z M 30.315677,39.58609 c 2.44673,-0.795863 4.805107,-1.095655 7.259206,-1.058461 -1.933408,-0.737186 -0.150022,-2.983692 -3.632769,-2.172821 -3.650741,0.03211 -9.489859,0.596224 -11.679659,-1.486504 4.477923,-0.08948 8.959,0.09953 13.334942,0.515327 3.155694,-2.270684 -0.427148,-6.412484 1.156732,-9.428048 0.833627,-4.812398 1.050084,-9.650634 0.713356,-14.492994 -0.449658,-2.8793667 1.097271,-5.792942 0.221093,-8.5999183 -0.235251,-2.79599833 -7.654859,-1.92512093 -7.253245,-2.57659253 1.760691,0.1507714 9.244695,-1.04663 3.751687,-0.7421257 -4.317041,-0.2539348 -10.517926,1.2816096 -13.441274,-1.43104457 -0.12275,-1.3896837 3.256987,-4.0263693 0.269308,-1.6663879 -3.071396,2.3360837 3.053459,4.51845127 4.45072,3.89681157 -6.876989,-0.7427124 -5.848844,4.83075463 -5.604742,8.03940633 0.08746,6.4650701 0.197578,12.9370581 0.495799,19.3911821 0.104963,3.056237 -1.85942,7.541619 2.871705,9.198538 2.534645,0.581015 5.291374,1.886586 7.087141,2.613629 z m -0.635982,-0.164529 c -1.850873,-1.598904 1.963783,-0.786849 0,0 z m -2.004628,-0.747266 c -0.05162,-1.134519 1.036923,0.599714 0,0 z m 3.567457,-0.333678 c -0.155569,-0.708965 0.957766,0.587348 0,0 z m -0.409547,-1.013916 c -2.975685,-0.152664 2.619323,-0.757051 0,0 z m 1.500072,-0.158287 c 0.355157,-0.75713 0.984042,0.510681 0,0 z M 33.45765,36.90835 c 0.356337,-0.430045 0.385306,0.283094 0,0 z m -2.743114,-2.331527 c 0.422769,-0.622343 0.204761,0.927615 0,0 z m -0.961935,-0.27268 c -4.489039,-0.366123 0.636717,-3.844036 0,0 z m -3.485586,-0.218538 c 0.40399,-0.324022 -0.08658,0.563702 0,0 z m 6.073464,-0.221653 c -0.664374,-1.067891 1.16169,-0.06498 0,0 z m -6.857281,-0.213078 c 0.214402,-1.833468 2.628296,0.488618 0,0 z m 8.912964,-0.396896 c -1.606424,-1.801765 2.841759,0.454616 0,0 z M 22.382025,31.600459 c 0.231206,-0.39734 0.213623,0.454912 0,0 z m 5.520775,-0.97303 c -0.125018,-1.811276 1.474256,0.07447 0,0 z m -7.823552,0.03706 c -1.298006,-1.471365 1.672693,-0.655055 0,0 z m 2.102253,-0.134727 c -0.356326,-0.738536 1.042101,0.66099 0,0 z m 4.956978,-0.3537 c -0.688831,-1.923214 0.752845,0.845962 0,0 z m -3.106527,-0.391554 c 0.380582,-0.189782 0.244961,-0.12215 0,0 z m 9.400947,-0.06003 c -1.261704,-0.790879 0.920478,-0.579231 0,0 z m 2.376191,0.08092 c 0.08862,-0.921559 0.822654,0.495055 0,0 z M 23.3906,29.042331 c -1.385853,-1.251045 1.443621,-0.638691 0,0 z m 3.318896,-0.592065 c 2.830794,-0.383758 3.64773,-3.170962 5.080937,-3.478871 2.230021,1.197938 -1.056271,0.584618 -0.354633,2.114564 -0.983137,1.103563 -3.053509,1.316236 -4.726304,1.364307 z m 7.268607,-0.423459 c 0.0624,-2.339645 2.490443,1.037997 0,0 z m -7.362824,-0.952525 c 0.547512,-1.256865 0.352448,-5.961741 2.377891,-2.55163 0.606173,1.263888 0.154576,3.303322 -2.377891,2.55163 z m 1.523311,-0.583617 c -0.480728,-0.699158 -0.360185,0.885375 0,0 z m 7.620447,0.02193 c -1.19285,-1.837925 1.31022,0.655839 0,0 z m -0.904154,-1.153357 c 0.472895,-0.706371 0.830789,0.585741 0,0 z M 30.003884,24.81045 c -1.918591,-1.018839 -0.859973,-5.735773 1.772151,-3.2564 -5.343078,-1.588101 1.972474,2.899559 -1.772151,3.2564 z m 3.913403,0.157032 c 0.07314,-0.868512 1.250664,0.367324 0,0 z M 21.644875,24.236299 c -1.028426,-0.919708 1.049174,-0.370101 0,0 z m 1.214258,0.04845 c 0.450702,-0.828969 0.675725,0.531423 0,0 z m 8.889491,-0.607987 c -1.810224,-1.63148 2.461065,0.124343 0,0 z m -7.628576,-0.346311 c 0.05649,-1.207092 1.555566,0.469091 0,0 z m -1.851616,-0.0981 c -0.650583,-1.305284 2.131122,-0.241455 0,0 z m 13.167843,-0.01312 c -1.325062,-2.018849 3.246648,-0.236095 0,0 z M 31.216553,22.45388 c 0.652055,-2.150348 2.984062,0.957684 0,0 z m -5.07596,-0.195264 c -5.136504,-2.86261 0.587585,-7.256182 -0.469656,-10.842139 -1.578384,-2.702233 -2.944965,0.874517 -1.923811,2.102826 -2.056903,0.697367 -1.828924,2.075716 -2.677,-0.208461 -0.431028,-2.800967 -0.513101,-7.2091571 0.280485,-9.1546333 0.291931,2.4294892 4.134218,2.0050725 2.785494,-0.2761719 -1.569717,-4.10698433 6.552256,-3.50021563 9.255589,-1.7450649 3.172591,2.7152333 -3.482001,5.424062 0.661456,8.1839021 0.322775,1.301699 -5.22617,3.724352 -0.537469,2.8608 4.027893,-1.235193 0.368567,5.572473 -2.658032,3.150289 -2.137731,0.810908 0.537821,3.120631 -2.768845,3.892673 -1.348672,0.44112 -0.222443,2.164016 -1.948211,2.03598 z m -0.620052,-1.688931 c -0.811675,-1.068867 -0.0019,0.88379 0,0 z m 2.245945,-4.150909 c 0.280126,-0.75907 -0.949545,0.279945 0,0 z m 5.043666,-0.485964 c -0.12738,-0.449865 -0.475948,0.256141 0,0 z m 0.952253,-0.156226 c 1.226167,-2.699155 -0.296767,-1.987756 0,0 z M 33.328154,15.1702 c 2.034938,-1.583316 -1.561776,0.403883 0,0 z m -2.987788,-0.459702 c 1.789912,-2.214774 -3.177022,-0.433029 0,0 z m 3.151365,-0.411461 c -0.60434,-0.233703 0.09546,0.534946 0,0 z m -2.38552,-2.235195 c -0.448198,-0.71334 -0.451135,0.489173 0,0 z m -2.409724,-1.221046 c 4.057326,-1.1850161 0.178249,-7.3133273 -0.705625,-3.2291329 -0.261258,1.1132832 -3.354463,4.2875329 0.705625,3.2291329 z m 2.205248,-0.304576 c -0.632341,-0.353246 0.270622,0.918932 0,0 z M 23.771881,9.410782 c 1.223857,-2.5333592 -2.407172,-0.1906254 -0.47843,0.65925 l 0.313666,-0.3018257 z m 8.010834,0.094823 c 0.681841,-1.800191 -1.609251,1.809021 0,0 z M 25.449123,8.6856191 c -0.5462,-0.4294812 -0.110548,0.7151933 0,0 z M 25.295596,7.6659364 c -0.348086,-1.6555898 -0.353848,1.4537128 0,0 z m 0.0054,-1.199287 c 0.335835,-2.063353 -0.674852,0.7697459 0,0 z m 6.759302,-1.9244405 c 0.263529,-0.8560664 -0.938118,0.5628831 0,0 z M 21.042438,22.353834 c 0.02999,-2.551979 2.996573,-1.201984 0,0 z m 13.357981,-1.856657 c -0.786722,-1.958295 3.070868,-0.08901 0,0 z m -13.09441,-0.675484 c -1.100504,-3.117516 3.560202,0.45927 0,0 z m 12.117483,-1.506108 c 0.242111,-0.55855 0.37624,0.39453 0,0 z M 21.439077,17.253906 c -0.664204,-0.503381 0.601952,-0.02028 0,0 z m 14.868925,0.01218 c 0.103988,-0.793846 0.890895,0.478638 0,0 z m -5.133685,-0.359092 c 0.11053,-0.715212 0.546209,0.429488 0,0 z m -8.424289,-0.36957 c -1.137461,-1.57096 2.313233,-0.178868 0,0 z m 12.885487,-0.249015 c -1.62629,-0.935898 1.797783,-3.310908 0.2529,-1.133188 1.278789,-1.923215 1.578349,2.209268 -0.2529,1.133188 z M 22.177544,15.56154 c 0.431626,-0.772025 0.04577,1.417345 0,0 z m 2.589996,-1.251219 c 0.242456,-0.553347 0.416233,0.429366 0,0 z m -0.408946,-3.279552 c 0.517133,-0.62322 0.520939,0.769294 0,0 z m 12.041526,6.18e-4 c 0.0026,-1.1554009 0.921467,0.741626 0,0 z m 0.522204,-1.3121477 c 0.373356,-0.5182684 0.373362,0.5182587 0,0 z M 34.671532,6.0239974 c -1.207041,-2.5029425 4.991513,-1.0314367 0.837584,-0.065246 -0.274416,0.070119 -0.549589,0.084664 -0.837584,0.065246 z M 23.268069,4.1024227 c -0.736276,-1.2959839 2.173334,-0.8092662 0,0 z M 21.995794,3.9884632 c -1.184306,-2.1038931 3.293467,-0.3382255 0,0 z M 36.865693,2.8152163 c -4.787555,0.100633 2.644738,-2.06911103 0,0 z m 28.93277,36.4227997 c -0.543888,-0.816265 -1.668005,0.163798 0,0 z M 40.580117,38.895517 c -0.437196,-1.919944 -2.243143,0.375388 0,0 z m 6.021063,-0.995556 c 0.129123,-1.4202 -1.650409,-0.181305 0,0 z m 26.74997,-0.407068 c 1.041684,-1.640485 -1.599534,1.092609 0,0 z m 9.892838,-0.104719 c 2.164608,-2.127945 -2.049965,0.09688 -0.21798,0.06882 z m -0.352336,-0.359537 c 0.265301,-2.18916 0.692404,0.8303 0,0 z m -2.722826,0.539132 c 1.478389,-2.039244 -2.109403,0.245039 0,0 z M 41.806956,37.33665 c 0.388788,-1.137677 -1.273461,-0.01556 0,0 z m -0.454383,-0.135701 c -1.030141,-0.311702 0.705042,-0.156957 0,0 z m -23.537118,-0.03827 c -0.5702,-0.326633 0.08882,0.629137 0,0 z M 43.47079,35.901207 c -0.867974,-0.301654 1.433983,1.765588 0,0 z m -3.439877,0.294204 c -1.312141,-0.540949 -2.567287,0.658816 0,0 z m 31.356482,-0.394547 c -0.535647,-0.611518 -0.53563,0.611518 0,0 z m 10.662336,-0.976449 c -0.659806,-1.73754 -1.271503,1.062957 0,0 z m -1.97428,0.03027 c -0.578615,-1.447439 -1.809138,0.980792 0,0 z m 4.771375,-0.169177 c 1.482747,-1.652432 -2.747811,0.829292 0,0 z M 12.958914,33.316622 c 1.165314,-1.740297 -0.0579,-5.853273 -0.22893,-6.23685 -0.04255,2.075143 -0.68448,4.209403 0.22893,6.23685 z m 74.146869,0.115271 c -0.09688,-1.21988 -0.34413,-3.895803 -0.31232,-1.206767 l 0.07126,0.61519 z M 72.886861,31.815222 c -0.274395,-1.140714 -1.026479,0.582234 0,0 z M 18.224407,31.198195 c -0.7676,-0.669664 -0.446875,0.818432 0,0 z M 16.511063,30.35774 c -0.796845,-1.233739 0.10888,1.245242 0,0 z m -0.378238,0.0502 c -0.06195,-0.926935 0.636295,0.858447 0,0 z m 65.896139,0.114409 c 0.666281,-1.529276 -2.103451,0.273411 0,0 z m -0.599661,-0.159374 c -0.315153,-1.031967 1.238811,0.496385 0,0 z M 39.852774,30.177317 c -0.02517,-1.004876 -0.526753,0.682897 0,0 z m 19.137765,-0.520199 c -1.197259,-0.683176 0.49981,0.793261 0,0 z m -20.362022,-0.02879 c -0.40097,0.244272 0.686814,0.288158 0,0 z m 0.656617,-0.17705 c -0.904229,-0.931591 -1.058855,-0.171764 -0.04894,0.138345 z m 31.322145,-0.421263 c -0.915886,-1.120035 -0.566983,1.279469 0,0 z m 8.437132,0.161904 c -0.65892,-1.77362 -2.147111,1.111899 0,0 z m 1.572898,-0.489409 c 1.301596,-1.759193 -2.160451,1.110195 0,0 z m -0.642036,-0.02124 c -0.09548,-0.534946 0.60434,0.233677 0,0 z m 7.051893,-1.110462 c -0.384638,0.625795 0.355874,0.970672 0,0 z m -41.335214,0.122094 c -0.189411,-0.704161 -0.467189,0.279458 0,0 z m 19.288629,-0.348309 c 0.500102,-1.077759 -1.103934,-0.221818 0,0 z m 18.355536,-0.193055 c -0.219061,-0.762663 -0.813806,0.730151 0,0 z M 66.612033,26.337903 c -0.133397,-2.217219 -2.560138,0.463491 0,0 z M 65.745088,26.11091 c -0.650021,-1.277825 1.457403,1.304298 0,0 z m 17.564089,0.416646 c -0.112254,-0.667359 -0.624907,0.517833 0,0 z m 1.892463,-2.037617 c 0.177574,-7.824591 0.134764,-15.6504877 0.205129,-23.4757027 2.076177,-4.0732295 -3.112227,-7.5527187 -7.628759,-9.337821 -4.37806,-2.1840063 -9.881348,-3.9624753 -15.293945,-2.9115943 -3.455574,1.046519 -4.255656,4.7714507 0.236759,4.9386787 3.240717,2.7528193 8.276213,3.0425595 12.674943,3.7026118 2.360934,0.8559465 3.739126,2.02215337 5.123606,3.05827017 0.787871,-2.49253057 4.350101,-0.067214 1.221131,1.04382223 -1.53552,3.3734356 3.029349,6.5074026 1.498549,9.8399931 -2.08401,-1.6612619 -0.340807,2.960219 -1.214661,2.536376 4.898246,1.653641 -1.115568,5.811686 1.422164,8.392784 0.169534,0.358309 0.90435,3.790843 1.755084,2.212582 z m -0.531511,-0.454337 c -0.820896,-1.027809 1.35827,0.196622 0,0 z m -0.247571,-3.388019 c -1.333231,-1.600734 1.909895,-1.522351 0,0 z m 0.14914,-5.037747 c -1.461127,-1.065516 1.769713,-1.399196 0,0 z m 0.379956,-7.8422788 c -0.817984,-1.5567881 0.821783,0.2985814 0,0 z m -0.06899,-5.6618494 c 0.05098,-0.9323799 0.50527,0.557082 0,0 z m 0.163573,-2.95580593 C 81.62273,-4.4883659 74.805219,-3.9819134 69.533621,-4.8801879 c -1.799673,-0.04371 -3.6102,-2.5871568 -6.303325,-2.6098828 -2.788019,-2.274416 2.726747,-4.1107633 4.475636,-1.704437 2.272997,-0.311581 2.977983,-0.9034553 3.665213,1.001285 1.951502,1.5543172 2.42996,-1.317005 3.39834,0.644253 2.087601,0.6277689 0.272318,-1.875382 2.778138,-0.494764 3.896809,1.1071259 8.965796,3.8221755 7.498614,7.19363557 z M 83.519498,-3.2180473 c -0.09938,-0.4506536 -0.443186,0.2894909 0,0 z M 60.287271,-7.8526937 c 0.14528,-0.699039 0.720807,0.456955 0,0 z m 11.448244,-1.374303 c 0.659816,-2.8145113 3.752546,0.721744 0,0 z m 2.514539,-0.01012 c 0.443187,-0.289485 0.09938,0.450652 0,0 z m -2.314628,-1.4082843 c -0.04004,-0.58632 0.678123,0.6937963 0,0 z M 87.157359,24.49273 c -0.459026,0.384177 0.757521,0.49134 0,0 z m 0.0079,-3.515456 c -0.171726,1.144231 0.207766,1.464815 0,0 z m -74.702118,0.293041 c -0.508797,0.724893 0.51926,0.954263 0,0 z m 61.650662,1.155287 c 1.752937,-0.51021 -0.742995,-0.783456 0,0 z m -17.857407,-0.39821 c -0.213637,-0.45491 -0.231192,0.39735 0,0 z M 17.133888,21.441324 c -0.0045,-2.310994 -0.47994,1.268496 0,0 z m 30.670956,-0.201139 c -0.570202,-0.326621 0.08882,0.629147 0,0 z m 32.429722,-0.59491 c 1.503139,-2.268159 -2.612463,-0.185765 0,0 z m -0.531924,-0.47385 c 0.303239,-1.064969 0.303239,1.064985 0,0 z m -18.28824,0.255618 c -0.746628,-0.606038 -0.537115,0.786442 0,0 z M 72.74676,20.159131 c 1.688409,-1.362551 -1.949649,0.61137 0,0 z m 3.352863,-0.326102 c 2.984907,-1.967099 -2.602978,-1.530321 0,0 z M 53.085512,18.852707 c 0.133471,-1.077028 -0.803263,1.206665 0,0 z M 87.16519,16.830411 c -0.194648,1.282358 0.211481,0.976834 0,0 z m -71.085782,2.110424 c 0.255001,-0.824437 -1.045434,0.294501 0,0 z m 46.815037,-0.08809 c 2.423051,-0.312024 -2.124336,-0.126143 0,0 z M 18.545484,17.673058 c 0.0049,-1.586725 -0.613346,1.286717 0,0 z m -0.404922,0.111054 c -0.865576,-1.316603 0.887936,0.266894 0,0 z m 63.869936,0.05559 c -0.857202,-1.24546 -0.856151,1.159236 0,0 z m -5.988372,-0.362748 c -0.546197,-0.429498 -0.110529,0.715184 0,0 z M 40.032781,16.679222 c 0.291036,-0.780221 -0.813018,0.199961 0,0 z m 47.130878,-2.292974 c -0.375868,0.582459 0.364286,0.854844 0,0 z m -16.912843,0.681191 c 2.851329,-2.119757 -1.276191,-1.734754 -0.442605,-0.03794 z m 3.453948,-0.13111 c 1.640102,-0.724737 -0.501396,-0.790251 0,0 z M 16.016099,14.681647 c 0.388637,-1.177483 -1.712233,0.394886 0,0 z m -0.56305,-0.119058 c 0.912862,0.208777 0.243568,-0.413506 0,0 z m -2.989704,-1.448761 c -0.355058,1.030237 0.31048,0.713897 0,0 z m 68.166037,0.109574 c -0.648833,-2.472055 -2.462369,1.190333 0,0 z m -3.140825,-0.08127 c 0.362788,-2.142952 -2.216177,1.357338 0,0 z M 38.871,13.03072 c 1.363102,-1.352787 -1.891001,1.067778 0,0 z M 87.153948,13.0161 c -0.395052,0.276108 0.600113,0.255349 0,0 z M 54.279824,12.161382 c -0.322546,-0.435578 -0.255878,0.547954 0,0 z M 42.922478,11.847193 c -0.693281,-0.0086 0.89527,0.527211 0,0 z m 30.890525,-0.04071 c -0.414489,-1.562792 -1.600678,0.67384 0,0 z m -8.124537,-0.30443 c 1.467776,-1.143411 -1.151876,-0.247605 0,0 z m 3.390537,-0.125543 c 0.01537,-1.307737 -1.459832,0.884817 0,0 z m 7.188491,-0.504512 c 1.34681,-2.0130818 -2.54206,-0.08553 0,0 z m 3.566087,-0.466822 c 2.074788,-3.369923 -4.105757,0.72017 0,0 z M 72.584924,9.9635418 c 1.131236,-3.2797789 -3.451573,-0.079265 0,0 z M 15.024499,9.9195137 c 0.350862,-1.3896349 -0.858329,-0.4466987 0,0 z m 26.230129,0.1932273 c 1.406346,-1.0077547 -1.258837,-0.2501283 -0.03046,0.0036 z M 83.158143,9.8689461 c -0.114953,-0.7955592 -0.673772,0.6434759 0,0 z M 82.931015,8.9195022 c -0.400982,0.244282 0.686818,0.2881676 0,0 z M 77.336653,8.594697 c -0.487351,-1.3362652 -1.616021,0.8956767 0,0 z M 81.77313,6.9841512 c 0.573494,-2.1229944 -1.87438,-0.503791 -0.635584,0.3469823 z m -1.98025,0.038461 c 0.136357,-1.738679 -1.700726,0.9219849 0,0 z m -6.362187,-0.183741 c -0.09623,-1.2159216 -1.460449,0.6324772 0,0 z M 51.753035,6.3534753 c -0.52883,-1.156689 -1.221584,0.8776644 0,0 z m -0.582907,-0.018556 c -0.348337,-0.6364714 1.04385,0.2765448 0,0 z m 29.55385,-0.3406996 c -1.176559,-1.0562415 -0.916739,1.1742552 0,0 z m -22.013901,0.067345 c 0.170058,-1.3018542 -1.54033,-0.0361 0,0 z M 81.5571,5.2522591 c -0.799463,0.049932 0.683442,0.4247897 0,0 z m -4.108693,-0.08562 c -0.102864,-1.2829329 -0.78308,0.9591885 0,0 z M 17.815461,4.9328145 c -0.28876,-0.4613936 -0.28876,0.4613841 0,0 z M 71.6709,4.7769074 c -0.300409,-0.6432902 -0.864998,1.214902 0,0 z m 5.394708,-1.549347 c -0.926905,-1.1313274 -0.126443,0.8407494 0,0 z M 73.440126,2.767432 c 1.565665,-1.6871198 -2.260669,0.66853 0,0 z m -4.642715,0.1011391 c -0.45056,-1.90311623 -2.495965,0.322616 0,0 z M 12.459048,2.0756097 c -0.452811,0.3911942 0.725131,0.6043547 0,0 z m 65.633605,0.2927879 c 1.603908,-1.80791513 -3.098408,0.6253884 0,0 z M 77.112651,2.1200952 c -0.238561,-1.16498663 0.906233,0.256467 0,0 z M 61.5727,1.1231538 c 0.184435,-0.49709393 -1.328582,0.9589261 0,0 z M 87.298594,0.12189957 c -0.402691,0.4604733 0.690474,1.11410043 0,0 z m -11.767481,0.4996075 c 1.034778,-1.87695237 -2.629991,0.54827643 0,0 z M 47.466172,0.04385417 c 0.597982,-1.17266007 -1.208328,0.00675 0,0 z m -0.386075,0.038967 c -1.328903,-0.8060704 0.842403,-0.5422916 0,0 z m 24.859415,-0.6820289 c 0.455342,-2.37773877 -1.878401,0.9316115 0,0 z m -59.756177,-0.3473061 c -0.400971,0.2442707 0.686812,0.2881507 0,0 z m 7.379448,-0.55726757 c 0.187396,-1.5667165 -1.156436,0.83222477 0,0 z m -0.455587,-0.1155715 c -0.253643,-1.3311763 0.636136,0.87268237 0,0 z m -6.917678,-1.0427276 c -0.416484,0.5869065 0.398591,0.7669269 0,0 z m 28.104675,0.7939248 c -0.364734,-2.8832636 6.376503,-6.242205 1.885304,-8.5090213 -1.968585,-1.270112 -9.51168,-0.51096 -8.494626,0.255433 4.383978,-1.445136 13.332961,0.6218623 8.189338,4.6112775 -0.948131,0.966684 -3.252068,2.4603858 -1.580016,3.6423108 z m 26.022282,-0.154089 c -0.385592,-2.4845834 -3.718774,1.41457287 0,0 z M 15.48746,-2.5154888 c 0.612802,-1.4972142 -1.282317,1.310733 0,0 z m -0.398302,-0.1102109 c -0.295174,-1.2691152 0.763049,-0.5427021 0.215157,0.1359642 z M 37.831099,-2.29577 c -0.373366,-0.518274 -0.373358,0.5182758 0,0 z m 49.595486,-0.5128365 c -0.395061,0.2761175 0.600129,0.2553424 0,0 z M 67.366102,-2.9307007 c -1.066166,-1.3182715 -1.182799,1.1488524 0,0 z m -16.019144,0.073249 c -0.401632,-1.2380107 -2.006641,0.4120611 0,0 z m 10.904255,-0.118008 c -0.532899,-1.1786506 -1.138058,0.972575 0,0 z M 18.111439,-3.1412258 c 0.940576,-1.0973251 -1.360988,0.6502195 0,0 z m 9.657133,0.088393 c 3.391212,-1.5467941 13.99334,0.2245602 11.834016,-3.865945 -2.753505,1.8597273 -6.249286,-2.2310329 -9.702486,0.034113 -2.859387,0.9417048 -10.796484,3.7778776 -2.13153,3.8318453 z m 5.92763,-3.2479771 c 0.231214,-0.3973381 0.213616,0.4549178 0,0 z m 11.24602,1.1721973 c -0.288763,-0.461388 -0.288765,0.4613954 0,0 z M 24.648619,-5.7879522 c 0.08888,-0.4235808 -0.852443,1.0181682 0,0 z m 0.664191,-0.3663161 c 0.646913,-0.8204147 -1.502635,1.3094116 0,0 z m 33.806213,0.060785 c -0.336394,-0.9936969 -1.009925,0.4632735 0,0 z m 1.090519,-0.2531044 c -0.454037,-0.4138735 -0.109442,0.5934835 0,0 z M 26.7782,-6.8996534 c -1.337457,-1.3415153 -0.967531,1.4167734 0,0 z m -1.092493,-0.1433304 c 0.0013,-0.8045859 0.989498,0.38356 0,0 z m 59.333232,0.4154199 c -0.566416,-0.3644323 -0.967621,-0.6225694 0,0 z M 27.647555,-7.3508997 c 0.08888,-0.423588 -0.852443,1.0181686 0,0 z m 0.732348,-0.415157 c 0.355171,-0.660688 -1.141239,1.1431131 0,0 z m 0.954206,-0.488416 c 0.35517,-0.660695 -1.141239,1.1431132 0,0 z m 1.295,-0.634946 c 0.646911,-0.820409 -1.502637,1.309412 0,0 z M 18.49704,-9.4755207 c 1.574639,-1.3812663 -2.278543,1.689806 0,0 z m 62.568752,-0.09767 c -0.612869,-0.8545813 -0.07019,0.371772 0,0 z m -27.999164,-0.06466 c -0.02967,-1.1397513 -1.652434,0.311381 0,0 z m -0.490746,-0.185315 c 0.443187,-0.2894863 0.09938,0.450649 0,0 z M 20.132824,-10.550033 c 1.744133,-1.476053 -2.308972,1.6926873 0,0 z m 59.433503,0 c -0.612825,-0.854601 -0.07027,0.371765 0,0 z m -19.494653,-0.510124 c 1.015714,-0.688614 -0.561367,-0.39916 -0.338674,0.04307 z m 18.676761,0.03685 c -0.56642,-0.364425 -0.967627,-0.622562 0,0 z m -57.11614,-0.503555 c 0.646913,-0.820416 -1.502635,1.309404 0,0 z m 56.298248,0.01518 c -0.56642,-0.364432 -0.967623,-0.622562 0,0 z m -55.139572,-0.747795 c 1.436478,-1.305309 -2.364992,1.792266 0,0 z m 47.983234,0.19511 c 4.620944,0.590659 1.713034,-5.36734 -0.574956,-1.915888 -2.605065,1.509994 -7.651633,-1.990906 -9.648402,1.222741 1.00091,0.0105 4.67807,-1.203669 4.717478,0.0056 -3.533155,0.03025 5.204097,0.140875 5.50588,0.687579 z m -0.115703,-0.569357 c 1.884,-2.40838 4.709862,0.27146 0,0 z m 2.296534,-2.556261 c 0.242115,-0.558552 0.376235,0.394526 0,0 z m 4.021297,2.979343 c -0.61286,-0.854593 -0.07023,0.371767 0,0 z m -19.291507,-0.261034 c 0.892835,-0.912452 -1.406754,0.394302 0,0 z m -16.671809,-0.195902 c 2.633996,0.110305 2.699745,-0.72299 -0.107812,-0.134952 -1.736122,-0.05063 -5.553524,0.364419 -1.431144,0.276796 l 0.773787,-0.0472 z m -2.215918,-0.723708 c -1.687883,-0.926118 -6.55029,0.320895 -1.890673,0.08639 l 0.950567,0.0099 z m -7.356846,-0.192067 c 2.039943,-1.24593 9.057804,0.02894 8.221673,-0.713104 -1.552131,0.504547 -9.10362,-0.162711 -4.965878,-1.084909 2.337966,-0.43869 6.524429,0.955046 7.557798,0.273473 -3.130214,0.172701 -8.819403,-1.877869 -9.970251,0.780358 2.277035,0.200758 -2.705676,0.09119 -0.843342,0.744182 z m 12.35131,-0.193326 c -1.309132,-1.107333 -0.03496,0.446256 0,0 z m 32.051051,-0.161737 c -0.400971,0.244277 0.686823,0.288144 0,0 z m -32.977611,-0.04881 c -0.685673,-0.542791 -0.536054,0.328943 0,0 z m -5.048099,-0.773367 c -0.320496,-0.284541 -0.246242,0.279519 0,0 z m 1.434149,-0.0099 c -0.855958,-0.563116 -0.566068,0.337005 0,0 z m 29.682676,-0.240847 c -0.242462,-1.279315 -1.612971,0.254584 0,0 z m -9.678393,-0.08095 c -0.496427,-0.508666 -0.367848,0.602904 0,0 z m 16.450963,-0.213684 c -0.40097,0.244279 0.686813,0.288159 0,0 z m -36.900747,-0.521552 c 1.610017,1.715393 6.129321,-1.878785 2.732844,-1.940241 3.021065,2.913982 -6.298219,1.549754 -8.510004,1.786555 -2.951953,0.506048 -5.375699,-4.494658 -3.529369,-0.971383 2.262214,1.916207 6.276584,1.046306 9.306529,1.125069 z m 37.396458,-0.255666 c 2.638543,-1.490533 -1.630172,-3.275928 -3.206466,-1.72381 -1.727909,0.278194 -5.825505,-0.482169 -1.755149,-0.806474 3.837474,-0.374197 -4.49887,-0.623311 -0.789246,-0.228871 -2.983629,-0.302939 -6.842819,0.982282 -4.488813,1.662315 -1.7486,0.187784 -4.526366,0.887769 -1.045728,0.701089 3.774788,-0.184135 7.516844,0.37851 11.285402,0.395751 z m -1.13409,-0.503692 c -0.18406,-2.062648 -8.741048,0.437777 -4.578372,-1.170632 1.111982,0.179486 5.895742,-0.480268 4.578372,1.170632 z m -1.090881,0.177143 c -3.042681,-0.177612 -0.165185,-1.209798 0.694141,-0.272912 z m -8.722644,-1.314043 c -1.003611,-0.93644 3.012485,0.275531 0,0 z m 10.154314,-0.689723 c 0.519,-0.458222 0.270716,0.700004 0,0 z m -49.823271,2.547801 c -1.672118,-0.636391 -1.12312,0.445647 0,0 z m -1.945332,-0.262409 c -0.320496,-0.284546 -0.246245,0.279522 0,0 z m -1.372271,-0.551505 c -0.395067,0.276111 0.600133,0.255344 0,0 z m 14.616487,0.01556 c -1.339343,-0.61089 -1.884141,0.541867 0,0 z m 5.158358,0.0475 c -0.320496,-0.284541 -0.246244,0.279527 0,0 z m -19.774845,-0.844517 c -0.395067,0.276112 0.600133,0.255345 0,0 z m 39.713811,-0.02444 c 1.363172,-1.891848 -3.988939,0.822728 0,0 z m -24.021288,-0.238398 c -2.548578,0.601483 0.958145,-2.194986 -2.297686,-0.90277 2.023175,0.737774 -1.895935,0.9745 1.605002,0.982699 0.72377,0.04259 2.9379,-0.102939 0.692684,-0.07994 z m 21.573386,-0.431457 c -0.950634,-0.23685 0.273688,0.704868 0,0 z m -35.860821,-0.408181 c 2.731987,-1.059792 -3.230909,0.763699 0,0 z m 24.47271,-0.182917 c -1.558019,-0.165653 -8.798035,0.05711 -3.854897,0.05462 1.285169,-0.0094 2.570259,-0.02632 3.854897,-0.05462 z m 5.615316,-0.03304 c -1.783853,-0.08931 -3.001304,0.133303 0,0 z m -25.957348,-0.0952 c -1.508805,-0.427729 -0.847896,0.272843 0,0 z m 30.873986,0.0032 c -0.685669,-0.542783 -0.536052,0.328951 0,0 z m 1.15709,-0.0019 c -0.855976,-0.563116 -0.566069,0.337005 0,0 z m 6.4409,-0.02313 c -2.5485,-0.295719 -4.969204,0.242462 -1.214211,0.04277 0.405081,-0.0082 0.809443,-0.02918 1.214211,-0.04277 z M 41.39802,-19.287684 c 1.295976,-2.123822 0.665932,-4.847001 -3.406195,-3.845417 -3.599779,-0.272875 -13.275282,-1.162359 -11.43442,3.021773 3.8382,2.269927 9.400644,0.52224 13.949026,0.88275 z m 36.99351,-0.134052 c 3.323758,-3.664555 -4.365689,-5.036358 -8.137308,-4.433416 -1.764901,0.3031 -10.556782,-0.06571 -5.450682,1.961321 3.60969,2.377512 8.8369,3.281556 13.58799,2.472095 z M 22.313868,-24.27454 c -0.416224,-0.42938 -0.242469,0.553356 0,0 z m 8.733502,-0.245238 c 6.658173,-0.102564 13.330294,0.269302 19.977478,-0.141587 -9.352236,-0.104213 -18.718664,-0.252453 -28.05999,0.180929 2.689536,0.221016 5.388437,-0.06969 8.082512,-0.03934 z m 3.977909,-0.115159 c 0.573387,-0.492669 0.479923,0.308369 0,0 z m 17.686918,-0.03036 c -0.238494,-0.532159 -0.726674,0.742493 0,0 z m 2.553319,-0.0677 c -0.465605,-0.452616 -0.426398,0.29833 0,0 z m 1.98104,-0.0012 c -1.046486,-0.629602 -0.688541,0.342411 0,0 z"
						id="a" />
					<rect
						style="fill:#ffffff;fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:0.12876649;stroke-miterlimit:4;stroke-dasharray:0.38629948, 0.1287665;stroke-dashoffset:1.39842534;stroke-opacity:1"
						id="c"
						width="69.645622"
						height="19.698757"
						x="15.024499"
						y="3.3434711"
						rx="8.6604118"
						ry="0.6394816" />
					<text
						xml:space="preserve"
						style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:12.27728558px;line-height:1.25;font-family:'Lucida Console';-inkscape-font-specification:'Lucida Console';letter-spacing:0px;word-spacing:0px;fill:#ff5555;fill-opacity:1;stroke:none;stroke-width:0.2301991"
						x="15.674602"
						y="17.528713"
						id="b"><tspan
						id="tspan4073"
						x="15.674602"
						y="17.528713"
						style="font-size:12.27728558px;fill:#ff5555;stroke-width:0.2301991">Jamserver</tspan></text>
					</g>
				</g>
				</svg>

				<iframe src="jamstatus.html" width="500px" height="300" style="border:1px solid black;" style="display: inline-block">
				</iframe>

				</body></html>
			EOH

		touch /var/www/html/jamstatus.html
		chown jamulus /var/www/html/jamstatus.html

		echo "[INFO] \$(timestamp) mounting file share and default remote recording folder"

		# create permanent mount
		export smbCredentialFile=/opt/filesharejam.crt
		export resourceGroupName="${jamgroupname}"
		export storageAccountName="${jamstorename}"
		export fileShareName="${jamsharename}"
		export mntPath="/mnt/${jamstorename}/${jamsharename}"

		if [ ! -d \$mntPath ]; then 

			az login --identity

			mkdir -p \$mntPath

			if [ ! -d "/etc/smbcredentials" ]; then
				mkdir "/etc/smbcredentials"
			fi

			storageAccountKey=\$(az storage account keys list --resource-group \$resourceGroupName --account-name \$storageAccountName --query "[0].value" | tr -d '"')

			if [ ! -f \$smbCredentialFile ]; then
				echo "username=\$storageAccountName" | tee \$smbCredentialFile > /dev/null
				echo "password=\$storageAccountKey" | tee -a \$smbCredentialFile > /dev/null
			else 
				echo "[INFO] \$(timestamp) The credential file \$smbCredentialFile already exists, and was not modified."
			fi

			chmod 600 \$smbCredentialFile

			httpEndpoint=\$(az storage account show --resource-group \$resourceGroupName --name \$storageAccountName --query "primaryEndpoints.file" | tr -d '"')
				
			smbPath=\$(echo \$httpEndpoint | cut -c7-\$(expr length \$httpEndpoint))\$fileShareName

			if [ -z "\$(grep \$smbPath\ \$mntPath /etc/fstab)" ]; then
				echo "\$smbPath \$mntPath cifs nofail,vers=3.0,credentials=\$smbCredentialFile,serverino,uid=jamulus" | tee -a /etc/fstab > /dev/null
			else
				echo "[WARNING] \$(timestamp) /etc/fstab was not modified to avoid conflicting entries as this Azure file share was already present. You may want to double check /etc/fstab to ensure the configuration is as desired."
			fi

			mount -a
			
		else
			echo "[INFO] \$(timestamp) fileshare was already mounted"

		fi
		
		mkdir -p ${jamserverrecpath}

		echo "[INFO] \$(timestamp) activating services"
		
		systemctl daemon-reload
		systemctl enable jamulus.service
		systemctl start --no-block jamulus.service

		systemctl restart apache2.service

		echo "[INFO] \$(timestamp) test if everything went well"

		systemctl is-active jamulus.service >/dev/null 2>&1 && rc=0 || rc=1
		systemctl is-active apache2.service >/dev/null 2>&1 && rc=0 || rc=1
		echo \$rc

		exit \$rc

		EOINSTALL

	# this file helps to deploy the previously created file to the vm
	cat <<- EOINSTALL > ./public.json
		{ "script": "$(cat install_jamnode.sh | base64 -w0)", "timestamp": $runepoch }
		EOINSTALL

}

echo -e "\e[32m[INFO] $(timestamp) setting up working environment"

initinstaller

echo -e "\e[33m[INFO] $(timestamp) creating installer files"

createfiles

echo -e "\e[34m[INFO] $(timestamp) creating server and resources"

if [ $rrc == 0 ] || [ $force == 1 ] ; then 
	# change settings for resources here 

	# all resources are placed within this resource group
	az group create -l ${jamlocationname} -n ${jamgroupname} --only-show-errors

	# storage
	az storage account create -n ${jamstorename} -g ${jamgroupname} -l ${jamlocationname} --sku Standard_LRS --only-show-errors --encryption-services file --https-only --assign-identity

	az storage share create --account-name ${jamstorename} --name ${jamsharename} --only-show-errors

	# configuration of your virutal machine / server
	
	# Standard_F2s_v2 -> works, Standard_B1ls -> does not work, Standard_A1_v2
	az vm create -n ${jamvmname} -g ${jamgroupname} --public-ip-address-dns-name ${jamdnsname} --image ubuntults --size Standard_A1_v2 --generate-ssh-keys --assign-identity --scope /subscriptions/${jamsubscriptionid}/resourceGroups/${jamgroupname}/providers/Microsoft.Storage/storageAccounts/${jamstorename} --only-show-errors --nsg-rule NONE

	# "firewall" settings to allow to connect to your instance. if you want ssh access add a rule here (or change the az vm create command)
	az network nsg rule create --resource-group ${jamgroupname} --nsg-name ${jamvmname}NSG --name statusport --protocol Tcp --priority 1010 --destination-port-range 80 --only-show-errors

	az network nsg rule create --resource-group ${jamgroupname} --nsg-name ${jamvmname}NSG --name jammingport --protocol Udp --priority 1020 --destination-port-range 22124 --only-show-errors

	if [ $jamscheduled ]; then
		az deployment group create --resource-group ${jamgroupname} --template-file ./schedule_up.json
		az deployment group create --resource-group ${jamgroupname} --template-file ./schedule_down.json
		
		jamschedupid=$(az resource show --name "scheduledvmup" --resource-group ${jamgroupname} --resource-type "Microsoft.Logic/workflows" --query "identity.principalId" | tr -d '"')

		jamscheddownid=$(az resource show --name "scheduledvmdown" --resource-group ${jamgroupname} --resource-type "Microsoft.Logic/workflows" --query "identity.principalId" | tr -d '"')

		jamvmid=$(az vm list -g ${jamgroupname} --query "[].id" | tr -d '"[]\n ')

		az role assignment create --assignee $jamschedupid --role "Virtual Machine Contributor" --scope $jamvmid
		az role assignment create --assignee $jamscheddownid --role "Virtual Machine Contributor" --scope $jamvmid
	fi
fi

echo -e "\e[36m[INFO] $(timestamp) Infrastructure deployed, now installing / updating jamulus, this will take a while. Meanwhile, listen to some music http://ccmixter.org/people/tigabeatz"

az vm extension set --resource-group ${jamgroupname} --vm-name ${jamvmname} --name CustomScript --publisher Microsoft.Azure.Extensions --version 2.1 --settings ./public.json --protected-settings '{"managedIdentity":{}}' --no-wait

getout

echo "[INFO] $(timestamp) done. "
